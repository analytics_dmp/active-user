select *
from (
select 
hits.customDimensions.value as ssoid
,DATE_ADD(SEC_TO_TIMESTAMP(visitStartTime),7,"HOUR") as visit_start_time1
,hits.appInfo.screenName
,hits.appInfo.landingScreenName
,hits.eventInfo.eventCategory
,hits.eventInfo.eventAction
,hits.eventInfo.eventLabel
,hits.sourcePropertyInfo.sourcePropertyTrackingId
,hits.sourcePropertyInfo.sourcePropertyDisplayName
,FIRST(IF(hits.customDimensions.index=6, hits.customDimensions.value, NULL)) device_id
,FIRST(IF(hits.customDimensions.index=2, hits.customDimensions.value, NULL)) customer_value_2
,FIRST(IF(hits.customDimensions.index=3, hits.customDimensions.value, NULL)) customer_value_3
,FIRST(IF(hits.customDimensions.index=4, hits.customDimensions.value, NULL)) customer_value_4
,FIRST(IF(hits.customDimensions.index=5, hits.customDimensions.value, NULL)) customer_value_5
from 
[133600662.ga_sessions_intraday_20171010]   
where
--or  hits.customDimensions.index is null
 hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-13',  'UA-86733131-12', 'UA-86733131-11')
group each by 1,2,3,4,5,6,7,8,9
)
-- where  regexp_match(ssoid,r"^\d+$")
order by 2 desc 
limit 1000