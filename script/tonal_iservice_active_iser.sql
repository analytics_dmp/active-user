select *
from (
select 
hits.customDimensions.value as ssoid
,DATE_ADD(SEC_TO_TIMESTAMP(visitStartTime),7,"HOUR") as visit_start_time1
,hits.appInfo.screenName as screen_name
,hits.appInfo.landingScreenName
,hits.eventInfo.eventCategory
,hits.eventInfo.eventAction
,hits.eventInfo.eventLabel
,hits.sourcePropertyInfo.sourcePropertyTrackingId
,hits.sourcePropertyInfo.sourcePropertyDisplayName as app_name
,FIRST(IF(hits.customDimensions.index=6, hits.customDimensions.value, NULL)) device_id
,FIRST(IF(hits.customDimensions.index=2, hits.customDimensions.value, NULL)) customer_value_2
,FIRST(IF(hits.customDimensions.index=3, hits.customDimensions.value, NULL)) customer_value_3
,FIRST(IF(hits.customDimensions.index=4, hits.customDimensions.value, NULL)) customer_value_4
,FIRST(IF(hits.customDimensions.index=5, hits.customDimensions.value, NULL)) customer_value_5
from 
    TABLE_DATE_RANGE([133600662.ga_sessions_],
                    TIMESTAMP("2017-01-01"),
                    TIMESTAMP(DATE_ADD(CURRENT_DATE(),-1,'DAY')))
where
--or  hits.customDimensions.index is null
 hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-13',  'UA-86733131-11') -- 'UA-86733131-12',
group each by 1,2,3,4,5,6,7,8,9
)
where (screen_name  = 'Account Info TruemoveH' or screen_name like '%/Select Payment Method' )
-- where  regexp_match(ssoid,r"^\d+$")
order by 2 desc 